//JAVASCRIPT SYNCHRONOUS VS ASYNCHRONOUS 

/*
  SYNCHRONOUS JAVASCRIPT 
    - default 
    - only one operation is performed at a time 
    - operation doesn't continue until the one prior is finished 
    - stops when there is an error 
    - needs to process entire operation before moving on 
    - blocking: when execution of additinal JS processes must wait until the operation completes; makes for slower code 
*/
console.log('Hello World')
console.log('Hello Again')
for (let i = 0; i <= 15; i++){
  console.log(i)
}

console.log('Goodbye')

/* 
  ASYNCHRONOUS JAVASCRIPT
  - await 
  - executes code at the same time 
  - not occurring at the same time 
  - can classify most async js ops with 2 primary triggers 
    1. browser API/web API events or functions 
      - includes methods like setTimeout 
      - includes event handlers like onclicke, mouse over, scroll, etc

*/

// function printMe(){
//   console.log('print me')
// }

// //setTimeout(printMe, 5000) //delays the task by 5 seconds 

// function print(){
//   console.log('print meeee')
// }
// function test(){
//   console.log('test')
// }
// setTimeout(print, 4000)
// test()

// function f1(){
//   console.log('f1')
// }
// function f2(){
//   console.log('f2')
// }
// function main(){
//   console.log('main')
//   setTimeout(f1, 0)

//   new Promise((resolve, reject)=>
//     resolve('I am a promise')
//   ).then(resolve => console.log(resolve)) //promise comes before f1 bc 

//   f2() //comes first before f1 because f1 has a set timeout despite having 0 delay 
// }
// main()
/* 
  CALLBACK QUEUE - data structure 
  - in JS have a queue of callback funcs, it is called a callback queue or task queue
  - a queue data structure is 'First in first out"

  JOB QUEUE
  - job queue (promise) comes before callback queue (setTimeout)
  - higher priority than callback  
  - every time a promise occurs in the code, the executor function gets into the job queue
  - the event loop works as usual, to look into the queue items over the callback queue items when the stack is free 

  2. Promises (trigger)
  - unique JS object that allows us to perform async behavior 
  - the promises object the eventual completion or failure of an asynchronous operatio and its resulting value 
  - pending -> fulfill or reject 
  */

// new Promise((resolve, reject)=>
//   resolve('I am a promise') //promise to fulfil; does not move on to the then until promise is fulfilled 
// ).then(resolve => console.log(resolve))

/*  
  REST API using a json placeholder 
  - getting all posts (get method)
    - fetch method 
      - asynchronously requests for a resource/data 
      - syntax: 
        fetch('url', {optional objects}).then(response => response.json()).then(data)

        optional object which contains additional info about our requests such the method, the body, and the headers of our request 
    - used the promise for async 
*/

//getting all posts 

console.log(fetch('https://jsonplaceholder.typicode.com/posts')) // automatically read as a promise 
// a promise may be in one of 3 possible states: pending, fulfilled, or rejected 

fetch('https://jsonplaceholder.typicode.com/posts').then(response => response.json())//parse the response as JSON, convert data received from url 
.then(data=>{
  console.log(data)//results are processed of the fetch 
})
//can put inside a function 
/* 
  async await function 
  - normal function but makes the tasks inside asynchronous
  - can only use await keyword with async
*/
async function fetchData(){
//wait for the fetch method to compelete then stores the value in a variable name 
let result = await fetch('https://jsonplaceholder.typicode.com/posts')
console.log(result)
console.log(typeof result)
//converts the dat from the 'response' 
let json = await result.json()
console.log(json)
console.log(typeof json)

}
fetchData()

// retrieve a specific post 
fetch('https://jsonplaceholder.typicode.com/posts/1')
		.then(response => response.json()) //parse the response as JSON
		.then(data => {
			console.log(data) // process the results
		})

//creates a new post 
fetch('https://jsonplaceholder.typicode.com/posts', {method: 'POST', headers:{
  'Content-Type': 'application/json'
  },
  //sets the content/body of the 'request' object to be sent to the backend 
  body: JSON.stringify({
    title: 'New Post',
    body: 'Hello Again',
    userId: 2
  })
}).then(res=> res.json()).then(data=>console.log(data))

//updating a post - put method; updates whole doc (if want to update one part of the doc - use patch for partial updates ); modifying resource where the client sends data that updates the entire resource 
fetch('https://jsonplaceholder.typicode.com/posts/1', {method: 'PUT', headers:{
  'Content-Type': 'application/json'
  },
  //sets the content/body of the 'request' object to be sent to the backend 
  body: JSON.stringify({
    title: 'New Post',
    body: 'Hello Again',
    userId: 1
  })
}).then(res=> res.json()).then(data=>console.log(data))
//patch method applies partial updates to the resource 

//deleting a post 
fetch('https://jsonplaceholder.typicode.com/posts/1', {method: 'DELETE'}).then(res=> res.json()).then(data=>console.log(data))

//filtering a post - information sent via the url can be done by adding the question mark symbol 
/* syntax
    individual parameters 
    - 'url?parameterName=value'
    multiple parameters
    - 'url?parameterName=value&parameterB=valueB'
*/
fetch('https://jsonplaceholder.typicode.com/posts?userId=1').then(res=>res.json()).then(data=>console.log(data)) //add ?userId=1

//retrieve a nested/related comments to posts 
//retrieving comments for a specific post (/post/:id/comments)
fetch('https://jsonplaceholder.typicode.com/posts/2/comments').then(res=>res.json()).then(data=>console.log(data))