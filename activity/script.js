fetch('https://jsonplaceholder.typicode.com/todos')
.then(res => res.json())
.then(data=>{
  console.log(data)
})

// let mapArr = fetch('https://jsonplaceholder.typicode.com/todos').map(item=>{item.title}).then(res => res.json()).then(data=>{
//   console.log(data)
// })
// console.log(mapArr)

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then(res => res.json())
.then(data=>{
  console.log(data)
})


fetch('https://jsonplaceholder.typicode.com/todos', {method: "POST", headers: {'Content-Type': 'application/json'},
body:JSON.stringify({
  userId: 1,
  title: 'to do item',
  completed: false
})}).then(res=> res.json()).then(data=>console.log(data))

fetch('https://jsonplaceholder.typicode.com/todos/1', {method: "PUT", headers: {'Content-Type': 'application/json'},
body:JSON.stringify({
  userId: 1,
  title: 'Updated to do item',
  description: 'This is something I have to do today',
  status: 'Pending',
  dateCompleted: 'Pending'
})}).then(res=> res.json()).then(data=>console.log(data))

fetch('https://jsonplaceholder.typicode.com/todos/1', {method: "PATCH", headers: {'Content-Type': 'application/json'},
body:JSON.stringify({
  status: 'Completed',
  dateCompleted: '12/03/2021'
})}).then(res=> res.json()).then(data=>console.log(data))

fetch('https://jsonplaceholder.typicode.com/todos/1', {method: "DELETE"}).then(res=> res.json()).then(data=>console.log(data))